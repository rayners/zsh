# Used for reporting how load loading takes
zmodload zsh/datetime
start=$EPOCHREALTIME

autoload -Uz colors && colors

export ZSH_DISABLE_COMPFIX=true
export ZSH_DIR=$HOME/.zsh

export ZSH_PROFILE=${${ITERM_PROFILE:=default}:l}
# echo "PROFILE = $current_profile"
[[ -f "${ZSH_DIR}/profiles/${ZSH_PROFILE}.env.zsh" ]] && source "${ZSH_DIR}/profiles/${ZSH_PROFILE}.env.zsh"

#[[ -o interactive ]] && echo $fg_bold[green]"+++" $fg_no_bold[blue]"Reading" $fg[red]"~/.zshenv"

path=(
    ~/bin
    /usr/local/bin
    /usr/local/sbin
    $path
)
export PATH

export CLICOLOR=1

# default has dark blue on black for directories
# ouch my eyes!
# export LSCOLORS=gafxcxdxbxegedabagacad

# default LSCOLORS=exfxcxdxbxegedabagacad
export LSCOLORS=Gxfxcxdxbxegedabagacad

export JAVA_OPTS="-Xms1024m -Xmx2048m -XX:MaxPermSize=512m"
export MAVEN_OPTS="-Xmx1024m -DjasmineTimeout=1200 -XX:MaxPermSize=256m"

#export JAVA_6_HOME=`/usr/libexec/java_home -v1.6`
# export JAVA_7_HOME=`/usr/libexec/java_home -v1.7`
# export JAVA_8_HOME=`/usr/libexec/java_home -v1.8`

#export CATALINA_6_HOME="/usr/local/opt/tomcat6/libexec"
export CATALINA_7_HOME="/usr/local/opt/tomcat7/libexec"

export JAVA_HOME=$JAVA_8_HOME
export CATALINA_HOME=$CATALINA_7_HOME

export NVM_DIR="$HOME/.nvm"

# source: https://linuxtidbits.wordpress.com/2009/03/23/less-colors-for-man-pages/
# Less Colors for Man Pages
export LESS_TERMCAP_mb=$'\e[01;31m'       # begin blinking
export LESS_TERMCAP_md=$'\e[01;38;5;74m'  # begin bold
export LESS_TERMCAP_me=$'\e[0m'           # end mode
export LESS_TERMCAP_se=$'\e[0m'           # end standout-mode
export LESS_TERMCAP_so=$'\e[38;5;246m'    # begin standout-mode - info box
export LESS_TERMCAP_ue=$'\e[0m'           # end underline
export LESS_TERMCAP_us=$'\e[04;38;5;146m' # begin underline

export LEDGER_FILE=~/Dropbox/ledger.dat

[[ -f "$ZSH_DIR/secrets.zsh" ]] && source "$ZSH_DIR/secrets.zsh"

[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ '
# [[ ! $TERM == "dumb" ]] && [ -z "$TMUX" ] && TERM=xterm-256color
[[ ! $TERM == "dumb" ]] && TERM=xterm-256color

# temp workaround?
export PHANTOMJS_BIN=/usr/local/bin/phantomjs

end=$EPOCHREALTIME

#[[ -o interactive ]] && printf $fg_bold[green]"+++ "$fg_no_bold[blue]"Loaded  "$fg[red]"~/.zshenv"$fg[blue]" in "$fg[red]"%0.4f"$fg[blue]" seconds\n" $(($end-$start))
