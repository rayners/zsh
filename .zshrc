[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ ' && return

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Used for reporting how load loading takes
zmodload zsh/datetime
start=$EPOCHREALTIME

[[ -f "${ZSH_DIR}/profiles/${ZSH_PROFILE}.rc.zsh" ]] && source "${ZSH_DIR}/profiles/${ZSH_PROFILE}.rc.zsh"

autoload -Uz colors && colors

#echo -n $fg_bold[green]"+++" $fg_no_bold[blue]"Reading" $fg[red]""${${(%):-%x}/$HOME/\~}
#[[ -o interactive ]] && echo -n $reset_color" (for interactive use)"
#echo .

# Used for reporting how load loading takes
zmodload zsh/datetime
start=$EPOCHREALTIME

source $ZSH_DIR/aliases.zsh
source $ZSH_DIR/history.zsh
source $ZSH_DIR/fzf.zsh
source $ZSH_DIR/directories.zsh

fpath=(~/.zsh/functions /usr/local/share/zsh-completions $fpath)
autoload -U $ZSH_DIR/functions/*(:t)

# The following lines were added by compinstall
zstyle :compinstall filename '~/.zshrc'

autoload -Uz compinit
if [ $(date +'%j') != $(stat -f '%Sm' -t '%j' ~/.zcompdump) ]; then
  compinit -i
else
  compinit -i -C
fi
#compinit -i
# End of lines added by compinstall

function java7() {
    export JAVA_HOME=$JAVA_7_HOME
    export CATALINA_HOME=$CATALINA_7_HOME
}

# completion tweeks
# - case insensitive completion
zstyle ':completion:*' matcher-list '' 'm:{a-zA-z}={A-Za-z}'

# cache completion results
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

rbenv() {
    eval "$(command rbenv init -)"
    rbenv "$@"
}

hasCommand "rbenv" && eval "$(rbenv init -)"
# hasCommand "gulp"  && eval "$(gulp --completion=zsh)"

source $ZSH_DIR/nvm.zsh

if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi

# keys!
bindkey -e
bindkey '[C' forward-word
bindkey '[D' backward-word

export REPORTTIME=10

# prompt with promptinit!
autoload -U promptinit; promptinit
if [[ "$ITERM_PROFILE" == "Class" ]]; then
    prompt class
else
    prompt rayners
fi

[ -f /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

end=$EPOCHREALTIME

#printf $fg_bold[green]"+++ "$fg_no_bold[blue]"Loaded  "$fg[red]"~/.zshrc"$fg[blue]" in "$fg[red]"%0.4f"$fg[blue]" seconds\n" $(($end-$start))

export SSH_AUTH_SOCK=$HOME/.yubiagent/sock

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
source /usr/local/opt/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
