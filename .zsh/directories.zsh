# directories

setopt auto_cd
setopt auto_pushd
cdpath=($HOME/Code $HOME/Code/fswd $HOME/Code/ember )

# let's give autojump a try!
[[ -s $(brew --prefix)/etc/autojump.sh ]] && . $(brew --prefix)/etc/autojump.sh

[[ -s $(brew --prefix)/etc/profile.d/z.sh ]] && . /usr/local/etc/profile.d/z.sh
