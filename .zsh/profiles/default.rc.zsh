alias aop="JAVA_OPTS=\"-Xmx1024m -XX:MaxPermSize=1024m\" catalina.sh run"
alias make-aop="mvn -DskipTests clean compile war:exploded"

alias my-issues="jira-search \"assignee=currentUser() and statusCategory!=Complete\""

