# fzf setup

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Setting ag as the default source for fzf
export FZF_DEFAULT_COMMAND='ag -g ""'

export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:hidden --bind '?:toggle-preview'"
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"

fbr() {
    local branches branch
    branches=$(git branch | grep -v HEAD | fgrep -v '*' | awk '{print $1}')
    branch=$(echo "$branches" | fzf --preview="git log -1 {}")
    echo $branch
}

_fzf_complete_git() {
    ARGS="$@"
    if [[ $ARGS == 'git checkout'* ]]; then
        local FZF_COMPLETION_OPTS="+s -n 2 --with-nth=1..2 -d '\t' --ansi --preview='git log -1 --color=always {3}'"
        _fzf_complete "" "$@" < <(
            {
                (git branch -a |
                    fgrep -v HEAD |
                    fgrep -v '*' |
                    sed "s/.* //" |
                    sed -e 's#^remotes/[^/]*/\(.*\)#\1\t&#' |
                    sort -u |
                    awk '{if (!$2) print "\x1b[34;1mlocal branch\x1b[m\t" $1, "\t", $1; else print "\x1b[34;1mremote branch\x1b[m\t" $1, "\t", $2}') &
                (git tag | awk '{print "\x1b[31;1mtag\x1b[m\t" $1}')
            }
        )
    else
        eval "zle ${fzf_default_completion:-expand-or-complete}"
    fi
}

_fzf_complete_git_post() {
  cut -f2
}
