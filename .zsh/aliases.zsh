alias otbcheck="ag '{{\\s*(?!::)[^:\\n]*\\|\\s*translate\\s*}}'"

alias gp="git pull"
alias gpu="git push"
alias gpuu="git push -u origin \`git current-branch\`"
alias gpf="git push --force-with-lease"
alias o="open"
alias goops="git oops"
alias master="git checkout master"

alias mu4e="emacsclient -c --no-wait --eval '(mu4e)'"

# homebrew aliases
alias bup="brew update && brew outdated"
alias bug="brew upgrade"

# alias git="hub"
